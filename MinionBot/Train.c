//**************************************************************
//******** FUNCTIONS FOR TRAINING MINION BOT *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
//**************************************************************
#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#include "Train.h"

volatile unsigned char train_cmd[3];
//array for train command packet
volatile unsigned char train_chk[3];
//array for train command packet
int train_speed_flag;
//flag for accelerating and disccelerating
volatile unsigned char encoder[13];
// array used for encoder packet 
int check=0;


void recieve_train_cmd_packet()                   //recieves command packet for training
{	
	for (int f=0;f<=3;f++)
	{	
		train_cmd[f]=receiveByte();
		transmitByte0(train_cmd[f]);
	}
	
	for (int f=0;f<=3;f++)
	{	
		transmitByte0(train_cmd[f]);
	}
}

int check_packet()
{
	int temp=0;
	check=0;
	if (train_cmd[1]=='N' &&train_cmd[3]=='N')
	{
	for (int i=0;i<=5;i++)
	{
	switch (i)
	{
	case 1:
		if (check==0)
		{
		temp=0;
		train_chk[0]='F';
		train_chk[1]='N';
		train_chk[2]='F';
		train_chk[3]='N';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
		}		
		break;
	case 2:
		if (check==0)
		{
			temp=0;
			train_chk[0]='F';
			train_chk[1]='N';
			train_chk[2]='S';
			train_chk[3]='N';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
		}		
		break;
	case 3:
		if (check==0)
		{
			temp=0;
			train_chk[0]='B';
			train_chk[1]='N';
			train_chk[2]='B';
			train_chk[3]='N';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
		}		
		break;
	case 4:
		if (check==0)
		{
			temp=0;
			train_chk[0]='S';
			train_chk[1]='N';
			train_chk[2]='F';
			train_chk[3]='N';
			
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
			
		}		
		break;
	case 5:
		if (check==0)
		{
			temp=0;
			train_chk[0]='S';
			train_chk[1]='N';
			train_chk[2]='S';
			train_chk[3]='N';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
			
		}		
		break;
		}	
			
	}
if (check==1)
{
	return 1;
}
else
return 0;
}
else
{
	for (int i=0;i<=5;i++)
	{
	switch (i)
	{
	case 1:
		if (check==0)
		{
		temp=0;
		train_chk[0]='N';
		train_chk[1]='F';
		train_chk[2]='N';
		train_chk[3]='F';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
		}		
		break;
	case 2:
		if (check==0)
		{
			temp=0;
			train_chk[0]='N';
			train_chk[1]='F';
			train_chk[2]='N';
			train_chk[3]='S';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
		}		
		break;
	case 3:
		if (check==0)
		{
			temp=0;
			train_chk[0]='N';
			train_chk[1]='B';
			train_chk[2]='N';
			train_chk[3]='B';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
		}		
		break;
	case 4:
		if (check==0)
		{
			temp=0;
			train_chk[0]='N';
			train_chk[1]='S';
			train_chk[2]='N';
			train_chk[3]='F';
			
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
			
		}		
		break;
	case 5:
		if (check==0)
		{
			temp=0;
			train_chk[0]='N';
			train_chk[1]='S';
			train_chk[2]='N';
			train_chk[3]='S';
		for (int k=0;k<=3;k++)
		{
			if (train_cmd[k]==train_chk[k])
			{
				temp++;
			}
		}
		if (temp==4)
			{
				check=1;
			}
			
		}		
		break;
		}	
			
	}
if (check==1)
{
	return 1;
}
else
return 0;
}		
}



void execute_train_command()
{
	
	int left_speed=100,right_speed=100;
	
	get_max_speed( &left_speed,&right_speed ) ;
	if (train_cmd[1]=='N' && train_cmd[3]=='N')
	{
	if(train_cmd[0]=='F')
		{
			PORTC|=(1<<PC0);
			PORTC&=~(1<<PC1);
		}
		if(train_cmd[0]=='B')
		{
			PORTC|=(1<<PC1);
			PORTC&=~(1<<PC0);
		}
		if(train_cmd[0]=='S')
		{
			PORTC|=(1<<PC1);
			PORTC|=(1<<PC0);
		}
		if(train_cmd[2]=='F')
		{
			PORTC|=(1<<PC2);
			PORTC&=~(1<<PC3);
		}
		if(train_cmd[2]=='B')
		{
			PORTC|=(1<<PC3);
			PORTC&=~(1<<PC2);
		}
		if(train_cmd[2]=='S')
		{
			PORTC |=(1<<PC2);
			PORTC |=(1<<PC3);
		}
		
		OCR0=100;
		OCR2=100;
	}
	
	else
	{
		if(train_cmd[1]=='F')
		{
			PORTC|=(1<<PC0);
			PORTC&=~(1<<PC1);
		}
		if(train_cmd[1]=='B')
		{
			PORTC|=(1<<PC1);
			PORTC&=~(1<<PC0);
		}
		if(train_cmd[1]=='S')
		{
			PORTC|=(1<<PC1);
			PORTC|=(1<<PC0);
		}
		if(train_cmd[3]=='F')
		{
			PORTC|=(1<<PC2);
			PORTC&=~(1<<PC3);
		}
		if(train_cmd[3]=='B')
		{
			PORTC|=(1<<PC3);
			PORTC&=~(1<<PC2);
		}
		if(train_cmd[3]=='S')
		{
			PORTC |=(1<<PC2);
			PORTC |=(1<<PC3);
		}
		
		OCR0=100;
		OCR2=100;
	}		
			
}

void create_encoderpacket()                     //creates encoder packet for tx to PI
{
	unsigned int left ,right;
	calculate_sensor_values( &left,&right );
	unsigned int value;
	value=left ;
	encoder[6]='N';
	encoder[5]=((value%10)|0x30);
	value= value/10;
	encoder[4]=((value%10)|0x30);
	value= value/10;
	encoder[3] = ((value%10)|0x30);
	value = value / 10;
	encoder[2] = ((value%10)|0x30);
	value = value / 10;
	encoder[1] = ((value%10)|0x30);  
	value = value / 10;
	encoder[0] = ((value%10)|0x30);		
	
	
	value=left;
	encoder[13]='N';
	encoder[12]=((value%10)|0x30);
	value= value/10;
	encoder[11]=((value%10)|0x30);
	value= value/10;
	encoder[10] = ((value%10)|0x30);
	value = value / 10;
	encoder[9] = ((value%10)|0x30);
	value = value / 10;
	encoder[8] = ((value%10)|0x30);  
	value = value / 10;
	encoder[7] = ((value%10)|0x30); 	
	
	
	transmit_encoder();
}

void transmit_encoder()
{
	for(int j=0;j<=13;j++)
	{
		transmitByte(encoder[j]);
	}
}