//**************************************************************
//******** FUNCTIONS FOR 7 Segment DISPLAY *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************


#ifndef DISPLAY_LOAD_H_
#define DISPLAY_LOAD_H_

float calculate_load();
void display_digit( int digit_number , int val );
void display();
void get_max_load();


#endif /* DISPLAY_LOAD_H_ */