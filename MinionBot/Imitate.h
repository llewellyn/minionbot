//**************************************************************
//******** FUNCTIONS FOR IMITATE PATH *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************


#ifndef IMITATE_H_
#define IMITATE_H_


void get_controlpacket();
void execute_command();
void speed_control( char left,char right );
int check_imitate_packet();


#endif /* IMITATE_H_ */