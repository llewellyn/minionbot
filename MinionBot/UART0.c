/*
 * UART0.c
 *
 * Created: 6/8/2014 8:07:41 PM
 *  Author: llewellyn
 */ 

#define F_CPU 16000000
#include "UART_routines.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#define USART_BAUDRATE0 9600 // Baud Rate value
#define BAUD_PRESCALE0 (((F_CPU / (USART_BAUDRATE0 * 16UL))) - 1)

//**************************************************
//UART0 initialize
//baud rate: 9600  (for controller clock = 16MHz)
//char size: 8 bit
//parity: Disabled
//**************************************************
void uart0_init(void)
{
	UCSR0A = 0x00; //disable while setting baud rate
	UCSR0A = 0x00;
	UCSR0C = 0x06;
	UBRR0L = BAUD_PRESCALE0;    // Load lower 8-bits of the baud rate value into the low byte of the UBRR register
	UBRR0H = (BAUD_PRESCALE0 >> 8); // Load upper 8-bits of the baud rate value..
// into the high byte of the UBRR register
	UCSR0B = 0x18;
}

//**************************************************
//Function to receive a single byte
//*************************************************
unsigned char receiveByte0( void )
{
	unsigned char data, status;
	
	while(!(UCSR0A & (1<<RXC0))); 	// Wait for incomming data
	
	status = UCSR0A;
	data = UDR0;
	
	return(data);
}

//***************************************************
//Function to transmit a single byte
//***************************************************
void transmitByte0( unsigned char data )
{
	while ( !(UCSR0A & (1<<UDRE0)) )
		; 			                /* Wait for empty transmit buffer */
	UDR0 = data; 			        /* Start transmition */
}


//***************************************************
//Function to transmit hex format data
//first argument indicates type: CHAR, INT or LONG
//Second argument is the data to be displayed
//***************************************************
void transmitHex0( unsigned char dataType, unsigned long data )
{
	unsigned char count, i, temp;
	unsigned char dataString[] = "0x        ";

	if (dataType == CHAR) count = 2;
	if (dataType == INT) count = 4;
	if (dataType == LONG) count = 8;

	for(i=count; i>0; i--)
	{
		temp = data % 16;
		if((temp>=0) && (temp<10)) dataString [i+1] = temp + 0x30;
		else dataString [i+1] = (temp - 10) + 0x41;

		data = data/16;
	}

	transmitString ( dataString );
}

//***************************************************
//Function to transmit a string in Flash
//***************************************************
void transmitString_F0( char* string ) 
{
	while ( pgm_read_byte(&(*string)) )
		transmitByte( pgm_read_byte(&(*string++)) );
}

//***************************************************
//Function to transmit a string in RAM
//***************************************************
void transmitString0( unsigned char* string )
{
	while ( *string )
		transmitByte( *string++ );
}

void transmitWord0( int temp )                //convert to int values and set a flag to indicate negative or positive number.
{
	transmitByte0((temp&0XFF00)>>8);
	transmitByte0(temp&0X00FF);
}