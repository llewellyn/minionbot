//**************************************************************
//******** FUNCTIONS FOR SERIAL COMMUNICATION USING UART *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
//**************************************************************

#define F_CPU 16000000
#include "UART_routines.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#define USART_BAUDRATE 38400// Baud Rate value
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

//**************************************************
//UART0 initialize
//baud rate: 9600  (for controller clock = 16MHz)
//char size: 8 bit
//parity: Disabled
//**************************************************
void uart1_init( void ) 
{
	UCSR1A = 0x00; //disable while setting baud rate
	UCSR1A = 0x00;
	UCSR1C = 0x06;
	UBRR1L = BAUD_PRESCALE;    // Load lower 8-bits of the baud rate value into the low byte of the UBRR register
	UBRR1H = (BAUD_PRESCALE >> 8); // Load upper 8-bits of the baud rate value..
// into the high byte of the UBRR register
	UCSR1B = 0x18;
 }

//**************************************************
//Function to receive a single byte
//*************************************************
unsigned char receiveByte( void )
{
	unsigned char data, status;
	
	while(!(UCSR1A & (1<<RXC1))); 	// Wait for incomming data
	
	status = UCSR1A;
	data = UDR1;
	
	return(data);
}

//***************************************************
//Function to transmit a single byte
//***************************************************
void transmitByte( unsigned char data )
{
	while ( !(UCSR1A & (1<<UDRE1)) )
		; 			                /* Wait for empty transmit buffer */
	UDR1 = data; 			        /* Start transmition */
}


//***************************************************
//Function to transmit hex format data
//first argument indicates type: CHAR, INT or LONG
//Second argument is the data to be displayed
//***************************************************
void transmitHex( unsigned char dataType, unsigned long data )
{
	unsigned char count, i, temp;
	unsigned char dataString[] = "0x        ";

	if (dataType == CHAR) count = 2;
	if (dataType == INT) count = 4;
	if (dataType == LONG) count = 8;

	for(i=count; i>0; i--)
	{
		temp = data % 16;
		if((temp>=0) && (temp<10)) dataString [i+1] = temp + 0x30;
		else dataString [i+1] = (temp - 10) + 0x41;

		data = data/16;
	}

	transmitString ( dataString );
}

//***************************************************
//Function to transmit a string in Flash
//***************************************************
void transmitString_F( char* string )
{
	while ( pgm_read_byte(&(*string)) )
		transmitByte( pgm_read_byte(&(*string++)) );
}

//***************************************************
//Function to transmit a string in RAM
//***************************************************
void transmitString( unsigned char* string )
{
	while ( *string )
		transmitByte( *string++ );
}


