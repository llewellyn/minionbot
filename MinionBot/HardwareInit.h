//**************************************************************
//******** FUNCTIONS FOR INITIALISING HARDWARE *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************


#ifndef HARDWAREINIT_H_
#define HARDWAREINIT_H_

#define Buzzer_ON PORTD|=(1<<PD7)
#define Buzzer_OFF PORTD&=~(1<<PD7)


#define ATMEGA_TX_ENC_SET			PORTA|=0x10   
#define ATMEGA_TX_ENC_CLEAR			PORTA&=0xEF    
#define CMD_RX_ACK_SET				PORTA|=0x20     
#define CMD_RX_ACK_CLEAR			PORTA&=0xDF
#define PACKET_ACK_SET				PORTA|=0x40 
#define PACKET_ACK_CLEAR			PORTA&=0xBF
#define BOARD22_BIT_SET				PORTA|=0x80
#define BOARD22_BIT_CLEAR			PORTA&=0x7F
#define BOARD24_BIT_SET				PORTD|=0X01
#define BOARD24_BIT_CLEAR			PORTD&=0XFE

#define SEND_ENC_VAL_READ		((PINA&0x01)==0x01)     
#define BOARD11_BIT_READ		((PINA&0x02)==0x02)			
#define RX_IMITATE_PKT			((PINA&0x04)==0x04)
#define RX_CTRL_CMD_READ		((PINA&0x08)==0x08)    
#define BOARD21_BIT_READ		((PINE&0x80)==0x80)

#define LED_RED_ON				PORTG|=(1<<PG0)
#define LED_RED_OFF				PORTG&=~(1<<PG0)
#define LED_BLUE_ON				PORTG|=(1<<PG1)
#define LED_BLUE_OFF			PORTG&=~(1<<PG1)
#define LED_GREEN_ON			PORTG|=(1<<PG2)
#define LED_GREEN_OFF			PORTG&=~(1<<PG2)
#define LED_WHITE_ON			PORTG|=(1<<PG3)
#define LED_WHITE_OFF			PORTG&=~(1<<PG3)

#define Calib_switch_read		((PING&0x10)==0x10)


void init_timer1();
void init_timer3();
void init_timer0();
void init_timer2();
void init_ports();
void calculate_sensor_values( unsigned int *x,unsigned int*y );
void reset_encoder();
void callibrate_sensor();
int retrieve_callibrate();
void init_interrupt7();







#endif /* HARDWAREINIT_H_ */