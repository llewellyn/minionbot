/*
 * ADC_Sensors.c
 *
 * Created: 5/30/2014 10:07:58 PM
 *  Author: llewellyn
 */ 

#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#include "ADC_Sensors.h"



void ADC_init()
{
  ADCSRA = 0x00; //disable adc
  ADMUX  = 0x40;  //select adc input 0, ref:AVCC
  ADCSRA = 0x87; //prescaler:8, single conversion mode
  ADC_ENABLE;
}

int readVoltage( unsigned int channel )         // this function reads the analog voltage on ADC pins
{
   char i;
   unsigned int ADC_temp, ADCH_temp;
   unsigned int ADC_var = 0;

   ADMUX = 0x40 | channel;
           
    for(i=0;i<8;i++)             // do the ADC conversion 8 times for better accuracy 
    {
	 	ADC_START_CONVERSION;
        while(!(ADCSRA & 0x10)); // wait for conversion done, ADIF flag active
        ADCSRA|=(1<<ADIF);
		
        ADC_temp = ADCL;         // read out ADCL register
        ADCH_temp = ADCH;        // read out ADCH register        
		ADC_temp +=(ADCH_temp << 8);
        ADC_var += ADC_temp;      // accumulate result (8 samples) for later averaging
    }

    ADC_var = ADC_var >> 3;       // average the 8 samples

	if(ADC_var > 1023) ADC_var = 1023;
	
    return ADC_var;   
} 