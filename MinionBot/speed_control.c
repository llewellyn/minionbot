///**************************************************************
//******** FUNCTIONS FOR SPEED CONTROLL *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************


#define F_CPU 16000000                        //CPU running on 16Mhz
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stdlib.h>

#include "speed_control.h"

int max_speed_left=255;
int max_speed_right=255;

int obstacle_value=400;


void change_speed( int left,int right )
{
	OCR0 = (left / 100) * max_speed_left;
	OCR2 = (right / 100) * max_speed_right;
}

void correct_allignment( int temp_left,int temp_right )    // call only when going forward.
{
	int Speed_left,Speed_right,old_speed_left,old_speed_right;
	
	old_speed_left = max_speed_left;
	old_speed_right = max_speed_right;
	Speed_left = OCR0;
	Speed_right = OCR2;
	
	if (temp_left >= temp_right && max_speed_left == 255 )
	{
		max_speed_left--;
	}
	
	if (temp_right >= temp_left && max_speed_right == 255)
	{
		max_speed_right--;
	}
	
	if (temp_left >= temp_right && max_speed_right < 255 )
	{
		max_speed_right++;
	}
	
	if (temp_right >= temp_left && max_speed_left < 255)
	{
		max_speed_left++;
	}
	
	OCR0 = (Speed_left / old_speed_left) * max_speed_left;
	OCR2 = (Speed_right / old_speed_right) * max_speed_right;
}
/*used to retriev max speed of left and right motor*/
void get_max_speed( int *x , int *y )
{
	*x=max_speed_left;
	*y=max_speed_right;
}


/*reads sharp value and changes speed depending on obstacle*/

float obstacle_speed(int pin_no)
{
	int sharp1;
	float speed_const;
	
	sharp1=readVoltage(pin_no);
	
	if (sharp1<200)
	{
		speed_const=1;	
	}
	if (sharp1>obstacle_value)
	{
		speed_const=0;
	}
	if (sharp1 > 200 && sharp1 < obstacle_value)
	{
		speed_const= 1 - (sharp1 / obstacle_value);
	}
	
	return speed_const;	
}

