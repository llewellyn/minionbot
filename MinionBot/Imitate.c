//**************************************************************
//******** FUNCTIONS FOR IMITATE PATH *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************
#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#include "Imitate.h"

volatile unsigned int control[7];
//array used for control packet
volatile unsigned char motors[2];

int flag=0;
//Flag used for accel and deaacel
volatile unsigned int imitate_check[10];
int imitate_chk;

unsigned char temp[16];

void get_controlpacket()                //recieves control packet from PI to carry out imitate
 {	
	char Imitate;
	 
	while ((Imitate=receiveByte()) != 'I');
	
	for (int u=0;u<=15;u++)
	{
		temp[u]=receiveByte();
		//transmitByte0(temp[u]);
	}
	transmitByte0(temp[15]);
	
	if (temp[2]=='N' && temp[9]=='N')
	{
		motors[0]=temp[0];
		motors[1]=temp[1];
		control[0]=temp[0];
		control[1]=temp[1];
		control[2]=temp[2];
		control[3]=((100000*(temp[3]-48))+(10000*(temp[4]-48))+(1000*(temp[5]-48))+(100*(temp[6]-48))+(10*(temp[7]-48)+(temp[8]-48)));
		control[4]=temp[9];
		control[5]=((100000*(temp[10]-48))+(10000*(temp[11]-48))+(1000*(temp[12]-48))+(100*(temp[13]-48))+(10*(temp[14]-48))+(temp[15]-48));
	}			
	if (temp[13]=='N' && temp[6]=='N')
	{
		motors[0]=temp[15];
		motors[1]=temp[14];
		control[0]=temp[15];
		control[1]=temp[14];
		control[2]=temp[13];
		control[3]=((100000*(temp[12]-48))+(10000*(temp[11]-48))+(1000*(temp[10]-48))+(100*(temp[9]-48))+(10*(temp[8]-48)+(temp[7]-48)));
		control[4]=temp[6];
		control[5]=((100000*(temp[5]-48))+(10000*(temp[4]-48))+(1000*(temp[3]-48))+(100*(temp[2]-48))+(10*(temp[1]-48))+(temp[0]-48));
		
	}
 }	
	

/*executes imitate command*/
void execute_command()                              // this function is used to Imitate the Path
{
	int encoder_calc_l,encode_calc_r;
	int prev_enc_l, prev_enc_r;
	int left_speed,right_speed;
	OCR0=OCR2=0;
	get_max_speed( &left_speed,&right_speed ) ;
	
	calculate_sensor_values( &encoder_calc_l,&encode_calc_r );
	
	prev_enc_l= encoder_calc_l;                    //needed parameters for correcting allignment
	prev_enc_r= encode_calc_r;
	
	if(motors[0]=='F')                                                 // If F go Forwrd
		{
			PORTC|=(1<<PC0);
			PORTC&=~(1<<PC1);
		}	
		if(motors[1]=='F')                                                 
		{
			PORTC|=(1<<PC2);
			PORTC&=~(1<<PC3);
		}
		if(motors[0]=='B')                                                  // If B go backward
		{
			PORTC|=(1<<PC1);
			PORTC&=~(1<<PC0);
		}	
		if(motors[1]=='B')
		{
			PORTC|=(1<<PC3);
			PORTC&=~(1<<PC2);
		}	
		if(motors[0]=='S')                                               // If S stop
		{
			PORTC|=(1<<PC0);
			PORTC|=(1<<PC1);
		}	
		if(motors[1]=='S')
		{
			PORTC|=(1<<PC2);
			PORTC|=(1<<PC3);
		}
			
	while(((control[3])>encoder_calc_l) || ((control[5])>encode_calc_r))   // check if encoder values and packet encoder vales r not same
		{         	
		if (flag==0)
		{
			if (left_speed>right_speed)
			{
			for (int i=0;i<=right_speed;i++)
			{
				OCR0=i;
				OCR2=i;
				_delay_ms(10);
				flag=1;
			}
			OCR0=left_speed;
		}
		else
		{
			for (int i=0;i<=left_speed;i++)
			{
				OCR0=i;
				OCR2=i;
				_delay_ms(10);
				flag=1;
			}
			OCR0=right_speed;	
		}
		
	}
	else
	{
		speed_control( control[0],control[1] );
	}
			
		if((encoder_calc_l)>=control[3])
		{
			motors[1]='S';
			PORTC|=(1<<PC2);
			PORTC|=(1<<PC3);
		}
		if((encode_calc_r>=control[5]))
		{
			motors[0]='S';
			PORTC|=(1<<PC0);
			PORTC|=(1<<PC1);
		}
		calculate_sensor_values( &encoder_calc_l , &encode_calc_r );  
	}
	flag=0;
	PORTC=0xFF;	
}

/*change speed to depending on percentage of completiong of imitate.*/
void speed_control(char left,char right)
{
	int percent,enc_l,enc_r;
	
	if ((left=='F')&& (right='F'))
	{
		
		percent=(((enc_l/control[3]) + (enc_r/control[5])) / 2) * 100;
		if (percent<50)
		{
			change_speed( 100,100 );
		}
		if (percent>50&&percent<70)
		{
			change_speed(60,60);
		}
		if (percent>70&&percent<85)
		{
			change_speed(40,40);
		}
		if (percent>85&&percent<95)
		{
			change_speed(30,30);
		}
	}
	
	if ((left=='S') && (right='S'))
	{
		change_speed(0,0);
		flag=0;
	}
	if (left=='S'&& right =='F')                      //Turning at half the speed
	{
		change_speed(50,50);
	}
	if (left=='F'&& right =='S')
	{
		change_speed(50,50);
	}
}


int check_imitate_packet()
{
	int temp1;
	imitate_chk=0;
	if (temp[2]=='N' && temp[9]=='N')
	{
	for (int i=0;i<=5;i++)
	{
		switch (i)
		{
		case 1:
		if (imitate_chk==0)
		{
			imitate_check[0] = 'F';
			imitate_check[1] = 'F';
			imitate_check[2] = 'N';
			imitate_check[4] = 'N';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				if (temp1==4)
				{
					imitate_chk=1;
				}
			}		
			
			break;
			
			case 2:
				if (imitate_chk==0)
			{
			imitate_check[0] = 'B';
			imitate_check[1] = 'B';
			imitate_check[2] = 'N';
			imitate_check[4] = 'N';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
			
				if (temp1==4)
				{
					imitate_chk=1;
				}
			
		}			
			break;
			
			case 3:
				if (imitate_chk==0)
		{
			imitate_check[0] = 'S';
			imitate_check[1] = 'S';
			imitate_check[2] = 'N';
			imitate_check[4] = 'N';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				if (temp1==4)
				{
					imitate_chk=1;
				}
				}			
			
			break;
			
			case 4:
				if (imitate_chk==0)
		{
			imitate_check[0] = 'F';
			imitate_check[1] = 'S';
			imitate_check[2] = 'N';
			imitate_check[4] = 'N';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				if (temp1==4)
				{
					imitate_chk=1;
				}
			}			
			
			break;
			
			case 5:
				if (imitate_chk==0)
		{
			imitate_check[0] = 'S';
			imitate_check[1] = 'F';
			imitate_check[2] = 'N';
			imitate_check[4] = 'N';
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				
			if (temp1==4)
				{
					imitate_chk=1;
				}
			}
		}			
			
			break;
		}
	
	if (imitate_chk==1)
{
	return 1;
}
else
return 0;
}	
else
{
	for (int i=0;i<=5;i++)
	{
		switch (i)
		{
		case 1:
		if (imitate_chk==0)
		{
			imitate_check[0] = 'N';
			imitate_check[1] = 'N';
			imitate_check[2] = 'F';
			imitate_check[4] = 'F';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				if (temp1==4)
				{
					imitate_chk=1;
				}
			}		
			
			break;
			
			case 2:
				if (imitate_chk==0)
			{
			imitate_check[0] = 'N';
			imitate_check[1] = 'N';
			imitate_check[2] = 'B';
			imitate_check[4] = 'B';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
			
				if (temp1==4)
				{
					imitate_chk=1;
				}
			
		}			
			break;
			
			case 3:
				if (imitate_chk==0)
		{
			imitate_check[0] = 'N';
			imitate_check[1] = 'N';
			imitate_check[2] = 'S';
			imitate_check[4] = 'S';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				if (temp1==4)
				{
					imitate_chk=1;
				}
				}			
			
			break;
			
			case 4:
				if (imitate_chk==0)
		{
			imitate_check[0] = 'N';
			imitate_check[1] = 'N';
			imitate_check[2] = 'S';
			imitate_check[4] = 'F';
			
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				if (temp1==4)
				{
					imitate_chk=1;
				}
			}			
			
			break;
			
			case 5:
				if (imitate_chk==0)
		{
			imitate_check[0] = 'N';
			imitate_check[1] = 'N';
			imitate_check[2] = 'F';
			imitate_check[4] = 'S';
			if (motors[0]==imitate_check[0])
			{
				temp1++;
			}
			
			if (motors[1]==imitate_check[1])
			{
				temp1++;
			}
			if (control[2]==imitate_check[2])
			{
				temp1++;
			}
			if (control[4]==imitate_check[4])
			{
				temp1++;
			}
				
			if (temp1==4)
				{
					imitate_chk=1;
				}
			}
		}			
			
			break;
		}
	
	if (imitate_chk==1)
{
	return 1;
}
else
return 0;
}
}