//**************************************************************
//******** FUNCTIONS FOR TRAINING MINION BOT *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
//**************************************************************


#ifndef TRAIN_H_
#define TRAIN_H_


void transmit_encoder();
void create_encoderpacket();
void recieve_train_cmd_packet(); //Recive train command from Raspberry pi
void execute_train_command();  //Execute the train command
int check_packet();

#endif /* TRAIN_H_ */