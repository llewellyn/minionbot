//**************************************************************
//******** MAIN CODE *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************

#define F_CPU 16000000                        //CPU running on 16Mhz
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <avr/eeprom.h>

#include "HardwareInit.h"
#include "Train.h"
#include "ADC_Sensors.h"
#include "UART_routines.h"
#include "speed_control.h"
#include "Imitate.h"
#include "Display_Load.h"


/*
ISR(INT7_vect)
{
	PORTC=0XFF;
	_delay_ms(100);
}
*/
void initialise_hardware()
{
	init_ports();	
	init_timer3();
	init_timer0();
	init_timer1();
	init_timer2();
	uart1_init();
	uart0_init();
	ADC_init();
	reset_encoder();
	PORTC=0XFF;
	CMD_RX_ACK_CLEAR;
	ATMEGA_TX_ENC_CLEAR;
	Buzzer_ON;
	_delay_ms(1000);
	Buzzer_OFF;
	_delay_ms(1000);
}

int main(void)
{ 
	initialise_hardware();
	_delay_ms(10);
	int ack=0;
	unsigned int left,right;
	
    while(1)
    {	
		/*CMD_RX_ACK_CLEAR;
		ATMEGA_TX_ENC_CLEAR;
		Buzzer_OFF;
		
		if(RX_CTRL_CMD_READ)
		{
			ack=0;
			Buzzer_ON;
			_delay_ms(10);
			if(SEND_ENC_VAL_READ)
			{
				ATMEGA_TX_ENC_SET;
				_delay_ms(20);
				create_encoderpacket();
				ATMEGA_TX_ENC_CLEAR;
				while(SEND_ENC_VAL_READ);
			}
			reset_encoder();
			CMD_RX_ACK_SET;
			recieve_train_cmd_packet();
			ack = check_packet();
			if (ack==1)
			{
				_delay_ms(10);
				PACKET_ACK_SET;
				Buzzer_OFF;
				
			}
			else
			{
				recieve_train_cmd_packet();
			}				
			PACKET_ACK_SET;				
			CMD_RX_ACK_CLEAR;
			execute_train_command();
			while(RX_CTRL_CMD_READ);
			CMD_RX_ACK_CLEAR;
			PACKET_ACK_CLEAR;
		}	
		
		
		if(RX_IMITATE_PKT)
			{
				ack=0;
				Buzzer_ON;
				ATMEGA_TX_ENC_SET;
				reset_encoder();
				
				if (ack==1)
				{
					PACKET_ACK_SET;
					
				}
				else
				{
					_delay_ms(10);
					get_controlpacket();	
				}	
				
				PACKET_ACK_SET;
				ATMEGA_TX_ENC_CLEAR;
				
				execute_command();	
				Buzzer_OFF;
				ATMEGA_TX_ENC_SET;	
				while(RX_IMITATE_PKT);
				PACKET_ACK_CLEAR;
				ATMEGA_TX_ENC_CLEAR;
			}	
		
			/*if (Calib_switch_read)
			{
				callibrate_sensor();
			}*/
			
			calculate_sensor_values( &left,&right );
			
			if (right>=60)
			{
				Buzzer_ON;
			}
			else
			Buzzer_OFF;
					
	
			}			
}


