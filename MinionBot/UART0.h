/*
 * UART0.h
 *
 * Created: 6/8/2014 8:06:11 PM
 *  Author: llewellyn
 */ 


#ifndef UART0_H_
#define UART0_H_

#define CHAR 0
#define INT  1
#define LONG 2

#define TX_NEWLINE0 {transmitByte(0x0d); transmitByte(0x0a);}

void uart0_init(void);
unsigned char receiveByte0(void);
void transmitByte0(unsigned char);
void transmitString_F0(char*);
void transmitString0(unsigned char*);
void transmitHex0( unsigned char dataType, unsigned long data );




#endif /* UART0_H_ */