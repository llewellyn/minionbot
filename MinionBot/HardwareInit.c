//**************************************************************
//******** FUNCTIONS FOR INITIALISING HARDWARE *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************
/*  Timer 1 encoder for left wheel
	Timer0 PWM for left wheel
	timer 3 Encoder for Right wheel
	Timer 2PWM for right wheel
*/
	
#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

#include "HardwareInit.h"


int overflow_l=0,overflow_r=0;
//int values for wen the timer overflows.

unsigned int encoder_calc_l,encode_calc_r;
//int values for the encoder values calculated	

unsigned int callibrate;
//callibrate is used to gave the base value depending on gain of amplifier

int callib_Flag=0;
//used to indicate the if callibrate is used for first time 


ISR( TIMER1_OVF_vect )
{
	overflow_l++;
}

ISR( TIMER3_OVF_vect )
{
	overflow_r++;
}

void init_timer1()                       // timer 1 used for encoder 1 
{                                        //declared as a counter
	TCCR1A=0X00;                         // timer one used for left wheel
	TCCR1B=0X00;
	TCCR1C=0X00;
	TCCR1B=0X07;
	TIMSK|=(1<<TOIE1);
	ETIMSK|=(1<<TOIE3);
}

void init_timer3()                       //timer 3 used for encoder 2
{                                        // declared as a counter
	TCCR3A=0X00;                         // timer 3 used for right wheel
	TCCR3B=0X00;
	TCCR3C=0X00;
	TCCR3B=0X06;
	TIMSK|=(1<<TOIE1);
	ETIMSK|=(1<<TOIE3);
}

void init_timer0()                       //timer for motor control
{                                        // output on OC0 pin
	TCCR0=0X00;
	TCCR0|=	(1<<WGM00)|(1<<COM01)|(1<<CS01)|(1<<CS02);                         
}
void init_timer2()                        //timer for motor control
{                                        // output on OC2 pin
	TCCR2=0X00;
	TCCR2|=	(1<<WGM20)|(1<<COM21)|(1<<CS22);
}


/*void init_interrupt7()
{
	cli();
	EICRA=0X00;
	EICRB=0X00;
	EIMSK=0X00;
	
	EIMSK|=(1<<INT7);
	sei();
	
}*/
/*initialising the ports */
void init_ports(){
	
	DDRA=0XF0;                   //PortA Lower nibble RPI->AVR:   HIGHER nibble AVR->RPI
	DDRB=0XFF;                   // PortB lowwer nibble OutPut for Display: PB4 & PB7 PWM outputs : PB5 & PB6 O/p Display
	DDRC=0XFF;                   //Outputs for MOTORS.
	DDRD=0XB9;                   //PD7 O/p Buzzer: PD6 I/p Encoder: PD4 & PD5 O/p display : PD0 O/p AVR -> PI: PD3 O/p TXD
	DDRE=0X3C;                   // PE7 I/p PI -> AVR : PE6 I/p Encoder : PE 2-5 O/P Display 
	DDRF=0X00;                   //All inputs for ADC
	PORTF=0X00;
	DDRD&=~(1<<PD6);            //encoder PINS are inputs
	DDRE&=~(1<<PE6);
	DDRG=0X0F;                  //Lower nibble outputs for LED Status: PG4 input for callibration Switch
	DDRE|=(1<<PE1);
}

/*calculates the encoder values depends on the overflow */
void calculate_sensor_values( unsigned int *x , unsigned int *y )                  //this function calculated the sensor values if they overflow
{
	encoder_calc_l= ( overflow_l * 65535 ) + TCNT1;
	encode_calc_r= ( overflow_r * 65535 ) + TCNT3;
	*x = encoder_calc_l;
	*y = encode_calc_r;
}

/*resets the encoder value after each instruction*/
void reset_encoder()
{
	overflow_l=0;
	overflow_r=0;
	TCNT1=0;
	TCNT3=0;
}

void callibrate_sensor()           //this function used to callibrate the Load cell
{
	unsigned int temp;
	temp=readVoltage(3);
	while(temp<=900)              //making sure the gain of INA125 enough to give 5v output at full load condition
	{
		temp=readVoltage(3);
	}
	Buzzer_ON;                    //buzzer On indicated gain is set
	_delay_ms(1000);
	Buzzer_OFF;
	while(Calib_switch_read);          //removing the load and storing the BASE value under no load condition since gain has been increase
	
	callibrate=readVoltage(3);
	eeprom_update_word((uint16_t*)20,callibrate);            //save value of callibration in EEPROM
	_delay_ms(100);
	for (int i=0;i<=2;i++)
	{
	Buzzer_ON;                            //Buzzer beeps 2wice indicating callibration process complete
	_delay_ms(500);
	Buzzer_OFF;
	_delay_ms(500);
	}	
	
}

/*retrieves the callibrate value from eeprom*/
int retrieve_callibrate()
{
	if (callib_Flag==0)
	{
		callibrate=eeprom_read_word((uint16_t*)20);
		callib_Flag=1;
		return callibrate;
	}
	else
	return callibrate;
	
	
}
