///**************************************************************
//******** FUNCTIONS FOR SPEED CONTROL*******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************


#ifndef SPEED_CONTROL_H_
#define SPEED_CONTROL_H_


void change_speed( int left,int right );
void get_max_speed( int *x , int *y );
void correct_allignment( int temp_left,int temp_right ) ;
float obstacle_speed( int pin_no );


#endif /* SPEED_CONTROL_H_ */