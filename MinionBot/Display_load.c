///**************************************************************
//******** FUNCTIONS FOR 7 SEGMENT DISPLAY *******
//**************************************************************
//Controller		: ATmega128 (Clock: 16 Mhz-external)
//Compiler			: AVR-GCC (AVRStudio-5)
//Project Version	: DL_1.0
//Author			: llewellyn (inventrom)
//			  		  llewellyn.fernandes@inventrom.com
// DONE
//**************************************************************



#define F_CPU 16000000                        //CPU running on 16Mhz

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <avr/eeprom.h>
#include <math.h>

float load;

int max_Load,load_flag;
//Max load is the maximum capacity the Bot can handle. load flag is used to indicate if the load is being calculated for the first time
//hence retrieves the data from eeprom



/*Retrieves the Max_load from eeprom*/
void get_max_load()
{
	max_Load=receiveByte();
	eeprom_update_byte((uint8_t)40,max_Load);
}


/*calculates the load by using current reading and callibrated reading 
returns a value load which floating point number*/
float calculate_load()
{
	if (load_flag==0)
	{
		max_Load=eeprom_read_byte((uint8_t*)40);
		load_flag=1;	
	}
	int temp,callib;
	float load_cell_temp;
	callib= retrieve_callibrate();
	temp=readVoltage(3);
	load_cell_temp=( (float)(max_Load * (temp - callib) ))/((float)( 1023 - callib ));
	load= ceil( load_cell_temp * 10) / 10;
	
	display();
	return load;
}

/*Displays the Digits, all the necessary PINS r set in this function*/
void display_digit( int digit_number , int val )
{
	if (digit_number==1)
	{
		PORTB|=val;
	}
	
	if (digit_number==2)
	{
		PORTE|= ((val>>3)<<PE5);
		PORTE|= ((val>>2)<<PE4);
		PORTE|= ((val>>1)<<PE3);
		PORTE|= (val<<PE2);
	}
	
	if (digit_number==3)
	{
		PORTD|= ((val>>3)<<PD4);
		PORTD|= ((val>>2)<<PD5);
		PORTB|= ((val>>1)<<PB5);
		PORTB|= ((val)<<PB6);
		
		
		
	}
}

/*splits the integer into individual digit and calls the Display()function to display the digits*/
void display()
{
	int val,temp2;
	val=load*10;
	
	temp2 = ((val%10));
	val = val / 10;
	display_digit(1,temp2);
	temp2= ((val%10));  
	val = val / 10;
	display_digit(2,temp2);
	temp2 = ((val%10)); 
	display_digit(3,temp2);
	
}
