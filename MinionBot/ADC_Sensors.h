/*
 * ADC_Sensors.h
 *
 * Created: 5/30/2014 10:06:25 PM
 *  Author: llewellyn
 */ 


#ifndef ADC_SENSORS_H_
#define ADC_SENSORS_H_


#define ADC_ENABLE 					ADCSRA |= (1<<ADEN)
#define ADC_DISABLE 				ADCSRA &= 0x7F
#define ADC_START_CONVERSION		ADCSRA |= (1<<ADSC)

void ADC_init();
int readVoltage(unsigned int channel);


#endif /* ADC_SENSORS_H_ */